#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
"""
This Bot uses the Updater class to handle the bot.

First, a few callback functions are defined. Then, those functions are passed to
the Dispatcher and registered at their respective places.
Then, the bot is started and runs until we press Ctrl-C on the command line.

Usage:
Example of a bot-user conversation using ConversationHandler.
Send /start to initiate the conversation.
Press Ctrl-C on the command line or send a signal to the process to stop the
bot.
"""

from telegram import (ReplyKeyboardMarkup, ReplyKeyboardRemove)
from telegram.ext import (Updater, CommandHandler, RegexHandler, ConversationHandler)
import logging
import re
import os
import sqlite3
TOKEN = os.environ['TELEBOT_PCM_TOKEN']

# Enable logging
logging.basicConfig(format='%(asctime)s - %(name)s - %(levelname)s - %(message)s',
                    level=logging.INFO)

logger = logging.getLogger(__name__)

TRAIN_RECOG, TRAIN_ACT, STOP = range(3)

# init db
conn = sqlite3.connect('pcm.sqlite', check_same_thread=False)  # FIXME
c = conn.cursor()


def train(bot, update):
    reply = update.message.text
    answers_rex = '^(Empathique|Travaillomane|Persévérant|Promoteur|Rêveur|Rebelle)$'

    if re.match(answers_rex, reply):
        c.execute("SELECT rowid, question_answer FROM logs WHERE user_id = '{}' AND reply_timestamp is null ORDER BY question_timestamp DESC LIMIT 1".format(update.message.chat.id))
        conn.commit()
        rowid, question_answer = c.fetchone()
        c.execute("UPDATE logs SET reply_timestamp = CURRENT_TIMESTAMP, reply_value = '{}', correct = '{}' WHERE rowid = '{}'".format(reply, reply == question_answer, rowid))
        conn.commit()
        if reply == question_answer:
            update.message.reply_text('✅ Nice ! ')
        else:
            update.message.reply_text('❌ Et non, c\'etait un {}'.format(question_answer))

    reply_keyboard = [['Empathique', 'Travaillomane', 'Persévérant'], ['Promoteur', 'Rêveur', 'Rebelle'], ['Stop']]

    c.execute('SELECT rowid, * FROM pcm_recog ORDER BY RANDOM() LIMIT 1;')
    conn.commit()

    rowid, feature, pcm_type, description = c.fetchone()
    reply = 'Catégorie : ' + feature + ', \n' + description
    update.message.reply_text(reply,
                              reply_markup=ReplyKeyboardMarkup(reply_keyboard, one_time_keyboard=True))

    c.execute("INSERT INTO logs ('user_id', 'question_answer', 'question_type', 'question_id') VALUES ('{}', '{}','recog', {} )".format(update.message.chat.id, pcm_type, rowid))
    conn.commit()

    return TRAIN_RECOG


def cancel(bot, update):
    user = update.message.from_user
    logger.info("User %s canceled the conversation." % user.first_name)
    update.message.reply_text('Bye! I hope we can talk again some day.',
                              reply_markup=ReplyKeyboardRemove())

    return ConversationHandler.END


def error(bot, update, error):
    logger.warn('Update "%s" caused error "%s"' % (update, error))


def main():
    # Create the EventHandler and pass it your bot's token.
    updater = Updater(TOKEN)

    # Get the dispatcher to register handlers
    dp = updater.dispatcher

    # Add conversation handler with the states TRAIN_RECOG, TRAIN_ACT, LOCATION and BIO
    conv_handler = ConversationHandler(
        entry_points=[CommandHandler('start', train)],

        states={
            TRAIN_RECOG: [RegexHandler('^(Empathique|Travaillomane|Persévérant|Promoteur|Rêveur|Rebelle)$', train)],
            STOP: [RegexHandler('^(Stop)$', cancel)],

        },

        fallbacks=[CommandHandler('cancel', cancel)]
    )

    dp.add_handler(conv_handler)

    # log all errors
    dp.add_error_handler(error)

    # Start the Bot
    updater.start_polling()

    # Run the bot until you press Ctrl-C or the process receives SIGINT,
    # SIGTERM or SIGABRT. This should be used most of the time, since
    # start_polling() is non-blocking and will stop the bot gracefully.
    updater.idle()


if __name__ == '__main__':
    main()
