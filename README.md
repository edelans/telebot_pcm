# Purpose

## What is this repo for ?
A simple bot for practicing process communication model. The bot gives you a caracteristic and asks you to identify the related pcm type (Thinkers, Persisters, Harmonizers, Rebels, Imaginers).

## Process Communication Model (aka PCM)
[Process Communication Model](https://fr.wikipedia.org/wiki/Process_communication) is a communication framework developped by [Taibi Kahler](https://en.wikipedia.org/wiki/Taibi_Kahler) in the 70's. Its objective is to facilitate exchanges between people in the most common communication situations, especially in companies.  

This framework received a lot of fame because [this is how NASA used to hire its astronauts 20 years ago—and it still works today](https://qz.com/486740/this-is-how-nasa-used-to-hire-its-astronauts-20-years-ago-and-it-still-works-today/).

## Disclaimer
:warning: Please note that this is a pet project I put some energy in a weekend to experiment with various trending topics :

-   telegram bots,
-   elearning & gamification
-   on a subject that recently draw my attention (PCM)

In particular, I am not affiliated in anyway with official PCM consultants.

## Screenshot

![pcmbot](/uploads/08eee8c3e8fbc375de6129fcb0ef4eba/pcmbot.png)

# How to make it work

## Experiment

Open your telegram app and start a conversation with `pcmbot`. As this pet project is not meant to be reliable, if the bot does not answer, it's because he is broken / not online, sorry !


## Install

### localhost

    # clone this repo

    # create virtualenv
    mkvirtualenv --python=/usr/bin/python3 telebot_pcm

    # install dependencies
    pip install python-telegram-bot

    # export the telegram token for your bot (see telegram botfather docs to get your token)
    echo '# export telegram token as env variable for telebot_pcm' >> ~/.bashrc
    echo 'export TELEBOT_PCM_TOKEN=\'<your-telegram-bot-token>\'' >> ~/.bashrc

    # create your own db from the example db (provided with limited usecases)
    cp ./pcm.sqlite.dist ./pcm.sqlite


### server
additional steps for server setup

    # create virtualenv
    mkvirtualenv --python=/usr/bin/python3 telebot_pcm
    pip install python-telegram-bot

-   create git worktree

    mkdir /var/www/telebot_pcm


-   Tune the paths in telebot_pcm.service file, and your telegram bot token
-   Put the service file in `/etc/systemd/system/`
-   Make the right user owner of this file
-   Make systemd aware of your new service (to start on boot)

    sudo systemctl daemon-reload
    sudo systemctl enable telebot_pcm.service
    sudo systemctl start telebot_pcm.service

-   Push to deploy setup

    git remote add live ssh://deploy@eiffel:/var/repo/telebot_pcm.git

## Run

### commands

#### localhost

    workon telebot_pcm
    python telebot_pcm.py

#### server
Everythng is managed by the systemd service (start on boot, respawn when crashes...). You can check the log with journald :

    journalctl -u telebot_pcm.service  --since yesterday
    journalctl -u telebot_pcm.service  --since "2 minute ago"

### How does this work ?

-   the script leverage on the python-telegram-bot lib to long poll the telegram servers
-   when the servers as some new messages for your bot, the poll fetches it
-   and the script act upon it (or not)...


## Examples
Examples provided in `examples` directory are examples from the python-telegram-bot lib.
